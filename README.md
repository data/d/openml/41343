# OpenML dataset: winequality-red-8_vs_6

https://www.openml.org/d/41343

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

An imbalanced version of the Red Wine Quality data set. where the possitive examples belong to the class 8 and the negative examples belong to the class 6. (IR: 35.44)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41343) of an [OpenML dataset](https://www.openml.org/d/41343). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41343/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41343/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41343/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

